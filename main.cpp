#include "MLP.h"

using namespace std;

int main()
{
    int hL,hN,iN,oN,trainingSample,flag;
    bool bias;
    float lmse,momentum,teachingStep;
    char ifbias;

    cout<<"\nPodaj liczbe warstw: ";
    cin>>hL;
    cout<<"\nPodaj liczbe neuronow w kazdej warstwie: ";
    cin>>hN;
    cout<<"\nPodaj liczbe wejsc: ";
    cin>>iN;
    cout<<"\nPodaj liczbe wyjsc: ";
    cin>>oN;
    cout<<"\nUzyc wartosci progowej [T/N]";
    cin>>ifbias;

    if (ifbias == 'T'){
        bias = true;
    }
    else{
        bias = false;
    }
    MLP mlp(hL,hN,oN,iN,bias);
    system("cls");
    do{
        cout<<"\n[1] - Trenowanie sieci";
        cout<<"\n[2] - Wczytanie sieci z pliku";
        cout<<"\n[3] - Przetwarzanie w sieci danych z pliku";
        cout<<"\n[4] - Zapis sieci do pliku";
        cout<<"\n[5] - Wyjscie";
        cout<<"\n";
        cin>>flag;
        system("cls");

        switch(flag){
            case 1:
                cout<<"\nPodaj lmse: ";
                cin>>lmse;
                cout<<"\nPodaj wspolczynnik nauki: ";
                cin>>teachingStep;
                cout<<"\nPodaj momentum (0- jezeli ma byc nie uwzglednione): ";
                cin>>momentum;
                cout<<"\nPodaj liczbe probek w epoce: ";
                cin>>trainingSample;
                mlp.trainNetwork(teachingStep,lmse,momentum,trainingSample);
                break;
            case 2:
                mlp.loadNetwork();
                cout<<"\nWczytano siec";
                break;
            case 3:
                mlp.recallNetwork();
                break;
            case 4:
                mlp.saveNetwork();
                cout<<"\nZapisano siec";
                break;
            case 5:
                break;
            default:
                cout<<"\nWybierz opcje z menu";
                break;
        }
    }while(flag != 5);

    return 0;
}

#include <math.h>
#include <vector>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <fstream>

#define inputToHidden(inp,hid)                weights.at(inputN*hid+inp)
#define hiddenToHidden(toLayer,fromHid,toHid)   weights.at(inputN*hiddenN+ ((toLayer-2)*hiddenN*hiddenN)+hiddenN*fromHid+toHid)
#define hiddenToOutput(hid,out)               weights.at(inputN*hiddenN + (hiddenL-1)*hiddenN*hiddenN + hid*outputN+out)
#define _prev_inputToHidden(inp,hid)                prWeights.at(inputN*hid+inp)
#define _prev_hiddenToHidden(toLayer,fromHid,toHid)   prWeights.at(inputN*hiddenN+ ((toLayer-2)*hiddenN*hiddenN)+hiddenN*fromHid+toHid)
#define _prev_hiddenToOutput(hid,out)               prWeights.at(inputN*hiddenN + (hiddenL-1)*hiddenN*hiddenN + hid*outputN+out)
#define hiddenAt(layer,hid)                     hiddenNeurons[(layer-1)*hiddenN + hid]
#define outputDeltaAt(out)                      (*(odelta+out))
#define hiddenDeltaAt(layer,hid)                (*(hdelta+(layer-1)*hiddenN+hid))
#define sigmoid(value)  (1/(1+exp(-value)));
#define dersigmoid(value) (value*(1-value))

using namespace std;
class MLP
{
    private:
        vector<float>  inputNeurons;
        vector<float>  hiddenNeurons;
        vector<float>  outputNeurons;
        vector<float>  weights;
        bool bias;
        float random();
        int inputN,outputN,hiddenN,hiddenL;
        vector<float*> loadInput(string path, int columns, char sign);
        vector<int> loadGoal(string path);
        void populateInput(float tab[]);
        void calculateNetwork();
        double round(double x);

    public:
        MLP(int hiddenL,int hiddenN, int outputN,int inputN, bool biasN);
        ~MLP();
        bool trainNetwork(float teachingStep,float lmse,float momentum,int trainingSample);
        void recallNetwork();
        void saveNetwork();
        void loadNetwork();


};

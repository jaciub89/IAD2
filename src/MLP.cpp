#include "MLP.h"

//Multi-layer perceptron constructor
MLP::MLP(int hL,int hN, int oN, int iN,bool biasN){

    bias = biasN;
    outputN = oN;
    hiddenL = hL;
    if (bias == false){
        hiddenN = hN;
        inputN = iN;
    }
    else{
        hiddenN = hN + 1;
        inputN = iN + 1;
    }


    //let's allocate the memory for the weights
    weights.reserve(inputN*hiddenN+(hiddenN*hiddenN*(hiddenL-1))+hiddenN*outputN);

    //also let's set the size for the neurons vector
    inputNeurons.resize(inputN);
    hiddenNeurons.resize(hiddenN*hiddenL);
    outputNeurons.resize(outputN);

    //randomize weights for inputs to 1st hidden layer
    for(int i = 0; i < inputN*hiddenN; i++)
    {
        weights.push_back(random());//[-0.5,0.5]

    }



    //if there are more than 1 hidden layers, randomize their weights
    for(int i=1; i < hiddenL; i++)
    {
        for(int j = 0; j <  hiddenN*hiddenN; j++)
        {
            weights.push_back(random());//[-0.5,0.5]
        }
    }

    //and finally randomize the weights for the output layer
    for(int i = 0; i < hiddenN*outputN; i ++)
    {
         weights.push_back(random());//[-0.5,0.5]
    }

}
//destructor
MLP::~MLP(){
    weights.clear();
    inputNeurons.clear();
    hiddenNeurons.clear();
    outputNeurons.clear();
}
//propagate values through network
void MLP::calculateNetwork(){
    for(int hidden = 0; hidden < hiddenN; hidden++)
    {
            hiddenAt(1,hidden) = 0;

            for(int input = 0 ; input < inputN; input ++)
            {
                hiddenAt(1,hidden) += inputNeurons.at(input)*inputToHidden(input,hidden);
            }
            if(bias && (hidden == (hiddenN - 1))){
                hiddenAt(1,hidden) = 1; //bias for future layers
            }
            else{
                hiddenAt(1,hidden) = sigmoid(hiddenAt(1,hidden));
            }
    }
    for(int layer = 2; layer <= hiddenL; layer ++){
        for(int hidden = 0; hidden < hiddenN; hidden++){
            hiddenAt(layer,hidden) = 0;
            for(int k = 0; k <hiddenN; k++)
            {
               hiddenAt(layer,hidden) += hiddenAt(layer-1,k)* hiddenToHidden(layer,k,hidden);
            }
            if(bias && (hidden == (hiddenN - 1))){
                hiddenAt(layer,hidden) = 1;
            }
            else{
                hiddenAt(layer,hidden) = sigmoid(hiddenAt(layer,hidden));
            }
        }
    }
    for(int out = 0; out< outputN; out++){
        outputNeurons.at(out) = 0;
        for(int hidden = 0; hidden <hiddenN; hidden++)
        {
            outputNeurons.at(out) += hiddenAt(hiddenL,hidden) * hiddenToOutput(hidden,out);
        }
         outputNeurons.at(out) = sigmoid( outputNeurons.at(out));
    }
}
//assigns values to the input neurons
void MLP::populateInput(float tab[]){
    for(int input = 0; input < inputN ; input++){
        inputNeurons.at(input) = tab[input];
        if(bias && (input == ( inputN - 1))){
            inputNeurons.at(input) = 1; //input bias
        }
    }
}
//trains the network according to our parameters
bool MLP::trainNetwork(float teachingStep,float lmse,float momentum,int trainingSample){
    vector<int> goal = loadGoal("goal.txt");
    ofstream file("log.txt");
    vector<float*>input;
    if(bias){
        input = loadInput("test.txt",inputN - 1,';');
    }
    else{
        input = loadInput("test.txt",inputN,';');
    }

    float mse = 999.0;
    int tCounter = 0;
    int epochs = 1;
    float error = 0.0;
    float* odelta = (float*) malloc(sizeof(float)*outputN);
    float* hdelta = (float*)  malloc(sizeof(float)*hiddenN*hiddenL);
    std::vector<float> tempWeights = weights;
    std::vector<float> prWeights = weights;

   while(mse >= lmse)
    {
        mse = 0.0;
        while(tCounter < trainingSample)
        {
           populateInput(input[tCounter]);
           calculateNetwork();
           int target = goal[tCounter];
            for(int i = 0; i < outputN; i ++)
            {
                if(i != target)
                {
                     outputDeltaAt(i) = (0.0 - outputNeurons[i])*dersigmoid(outputNeurons[i]);
                     error += (0.0 - outputNeurons[i])*(0.0-outputNeurons[i]);
                }
                else
                {
                     outputDeltaAt(i) = (1.0 - outputNeurons[i])*dersigmoid(outputNeurons[i]);
                     error += (1.0 - outputNeurons[i])*(1.0-outputNeurons[i]);
                }
            }
            for(int i = 0; i < hiddenN; i++)
            {
                hiddenDeltaAt(hiddenL,i) = 0;
                for(int j = 0; j < outputN; j ++)
                {
                    hiddenDeltaAt(hiddenL,i) += outputDeltaAt(j) * hiddenToOutput(i,j);
                }
                hiddenDeltaAt(hiddenL,i) *= dersigmoid(hiddenAt(hiddenL,i));
            }
            for(int i = hiddenL-1; i >0; i--)
            {
                for(int j = 0; j < hiddenN; j ++)
                {
                    hiddenDeltaAt(i,j) = 0;

                    for(int k = 0; k <hiddenN; k++)//to
                    {
                       hiddenDeltaAt(i,j) +=  hiddenDeltaAt(i+1,k) * hiddenToHidden(i+1,j,k);
                    }
                    hiddenDeltaAt(i,j) *= dersigmoid(hiddenAt(i,j));
                }
            }
            tempWeights = weights;
            for(int i = 0; i < inputN; i ++)
            {
                for(int j = 0; j < hiddenN; j ++)
                {
                    inputToHidden(i,j) +=   momentum*(inputToHidden(i,j) - _prev_inputToHidden(i,j)) +
                                            teachingStep* hiddenDeltaAt(1,j) * inputNeurons[i];
                }
            }
            for(int i = 2; i <=hiddenL; i++)
            {
                for(int j = 0; j < hiddenN; j ++)
                {
                    for(int k =0; k < hiddenN; k ++)
                    {
                        hiddenToHidden(i,j,k) += momentum*(hiddenToHidden(i,j,k) - _prev_hiddenToHidden(i ,j,k)) +
                                                 teachingStep * hiddenDeltaAt(i,k) * hiddenAt(i-1,j);
                    }
                }
            }
            for(int i = 0; i < outputN; i++)
            {
                for(int j = 0; j < hiddenN; j ++)
                {
                    hiddenToOutput(j,i) += momentum*(hiddenToOutput(j,i) - _prev_hiddenToOutput(j,i)) +
                                           teachingStep * outputDeltaAt(i) * hiddenAt(hiddenL,j);
                }
            }
            prWeights = tempWeights;
            mse += error/(outputN+1);
            error = 0;
            tCounter++;
        }
        if (epochs == 1){
            file<<epochs<<"\t"<<mse<<"\n";
        }
        tCounter = 0;
        char reply;
        if((epochs%1000) == 0)
        {
            cout<<"\nEpoka "<<epochs<<", kontynujemy nacisnij N, aby przerwac\nosiagniety poziom bledu :"<<mse;
            cin>>reply;
        }
        if((epochs%100) == 0)
        {
            file<<epochs<<"\t"<<mse<<"\n";
        }
        if(reply == 'N')
            break;

        epochs++;
    }
    cout<<"\nEpoka "<<epochs<<", osiagniety poziom bledu :"<<mse<<" koniec treningu.";
    file<<epochs<<"\t"<<mse<<"\n";
    file.close();
    return true;
}
//read values from source file and then propagate through network
void MLP::recallNetwork(){
    vector<float*>input;
    if(bias){
        input = loadInput("source.txt",inputN - 1,';');
    }
    else{
        input = loadInput("source.txt",inputN,';');
    }
    for(int i = 0 ; i < input.size(); i++){
        //first populate the input neurons
        populateInput(input[i]);

        //then calculate the network
        calculateNetwork();

        float winner = -1;
        int index = 0;

        //represent results
        cout<<"\nProbka nr: "<<i<<"\n";
        float* tab = input[i];
        int counter;
        if (bias){
            counter = inputN - 1;
        }
        else{
            counter = inputN;
        }
        for(int i = 0 ; i < counter; i++){
            cout<<tab[i]<<"\t";
        }
        for(int i = 0; i < outputN; i++)
        {
            cout<<"\nNeuron: "<<i<<"-"<<round(outputNeurons[i]*100)<<"%";

        }
        getchar();
        system("cls");
    }
}
//random number between -0.5,0.5
float MLP::random(){
    int n = rand() % 100 - 50;
    return n / 100.0f;
}
//loading input data from CSV file
vector<float*> MLP::loadInput(string path, int columns, char sign){
    fstream file;
    file.open(path.c_str());
    vector<float*>result;
    string data,tmp,type;
    if(file.is_open()){
        while(getline(file,data,sign)){
                float* tab = new float[columns];
                tab[0] = atof(data.c_str());
                for(int i = 1; i < columns; i++){
                    if (i == (columns-1)){
                        getline(file, tmp);
                    }
                    else{
                        getline(file, tmp, sign);
                    }
                    tab[i] = atof(tmp.c_str());
                }
                result.push_back(tab);
        }
        file.close();
    }
    else{
        cout<<"error - blad otwarcia: " + path + "\n";
    }
    return result;
}
//loading goal data for network training
vector<int> MLP::loadGoal(string path){
    fstream file;
    file.open(path.c_str());
    vector<int>result;
    string data;
    if(file.is_open()){
        while(getline(file,data)){
            int tmp = atoi(data.c_str());
            result.push_back(tmp);
        }
        file.close();
    }
    else{
        cout<<"error - blad otwarcia: " + path + "\n";
    }
    return result;
}
//saving network weigts to file
void MLP::saveNetwork(){
    ofstream file("network.txt");
    if (file.is_open()){
        for(int i = 0 ; i < weights.size() ; i++){
            file<<weights[i]<<"\n";
        }
    }
    else{
        cout<<"error - blad otwarcia";
    }

}
//saving network weigts to file
void MLP::loadNetwork(){
    fstream file;
    string data;
    file.open("network.txt");
    if (file.is_open()){
        while(getline(file,data)){
            float tmp = atof(data.c_str());
            weights.push_back(tmp);
        }
        file.close();
    }
    else{
        cout<<"error - blad otwarcia";
    }

}
//rounds to two digits after 0
double MLP::round(double x){
    return roundf(x * 100) / 100;
}
